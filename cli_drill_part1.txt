1) Create the following directory structure. (Create empty files where necessary)
hello
├── five
│   └── six
│       ├── c.txt
│       └── seven
│           └── error.log
└── one
    ├── a.txt
    ├── b.txt
    └── two
        ├── d.txt
        └── three
            ├── e.txt
            └── four
                └── access.log


Creating Directories---------
Create root directory "hello" 		: 	mkdir hello
Create directory "five" inside "hello" 	: 	mkdir hello/five
Create directory "six" inside "five" 	: 	mkdir hello/five/six
Create directory "seven" inside "six" 	: 	mkdir hello/five/six/seven
Create directory "one" inside "hello" 	: 	mkdir hello/one
Create directory "two" inside "one" 	:	mkdir hello/one/two
Create directory "three" inside "two" 	: 	mkdir hello/one/two/three
Create directory "four" inside "three"	: 	mkdir hello/one/two/three/four

Creating text files inside directories--------
Create text file "c.txt" inside directory "six" 	: 	touch hello/five/six/c.txt
Create text file "a.txt" inside directory "one" 	:	touch hello/one/a.txt
Create text file "b.txt" inside directory "one" 	:	touch hello/one/b.txt
Create text file "d.txt" inside directory "two" 	:	touch hello/one/two/d.txt
Create text file "e.txt" inside directory "three" 	:	touch hello/one/two/three/e.txt

Creating log files inside directories---------
Create log file "error.log" inside directory "seven" 	: 	touch hello/five/six/seven/error.log
Create log file "access.log" inside directory "seven" 	:	touch hello/one/two/three/four/access.log

2) Delete all the files with the .log extension		: 	find -name *.log -delete

3) Add the following content to "a.txt"			       

cd hello/one
echo "Unix is a family of multitasking, multiuser computer operating systems that derive from the original 
AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, 
Dennis Ritchie, and others.">>a.txt

4) Delete the directory named "five" 	: 	cd hello
						rm -r five

